--[[
	This script can be used to build a notification hook for dunst, it will take the envoirenment variables supply by dunst and form ddb instruction wich can be piped into a socket after the greeting.
]]--

local notification_broker = ...

local urgency_map = {
	LOW = "low",
	CRITICAL = "high",
}

local name = "dunst_notification_"..(os.getenv("DUNST_ID") or tostring(math.random()):sub(3)):gsub(" ","-"):gsub("\n", "")
local category = (os.getenv("DUNST_CATEGORY") or ""):gsub("\n"," ")
local description = (os.getenv("DUNST_BODY") or ""):gsub("\n"," ")
local title = (os.getenv("DUNST_SUMMARY") or ""):gsub("\n"," ")
local urgency = os.getenv("DUNST_URGENCY")
if urgency then
	urgency = urgency_map[urgency]
end
local progress = tonumber(os.getenv("DUNST_PROGRESS"))

print("> "..name.." type notification")
print("> "..name.." title "..title)
print("> "..name.." description "..description)
print("> "..name.." time_last_updated "..os.time())
print("> "..name.." category "..category)
if urgency then
	print("> "..name.." urgency "..urgency)
end
if progress then
	print("> "..name.." progress "..progress)
end

print("s "..notification_broker.." notify:"..name)
