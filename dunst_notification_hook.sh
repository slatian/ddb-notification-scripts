#!/bin/bash
# Replace 5678 and test with what you configured for your ddb server
# The exni stands for EXample Notification Inbox, wich is what I use for testing,
# You can chane the name to anything hat doesn't include spaces (you have to reconfigure the rest of the notification system to match that of course.
# Make sure that the path to the dunst_notification_serializer.lua is still correct

ddb_connector.sh localhost 5678 test "lua $HOME/.scripts/dunst_notification_serializer.lua exni"
