# Notification Infrastructure:

This file is copyed from my notes, may or may not be improved later.

## Notification Inbox:
### active_list
A space seperated list of active notifications

### inactive_list
A list of dismissed or hidden notifications

## Notification
### application_id
An identifier that identifies the application the notification came from

### title
A title that in one short line describes what the notification is about

### description
A longer oneliner that provides additional information to the title

### urgency
An application defined urgency level, one of:
* low
* normal (default)
* critical

### is_hidden
One of true or false (default).

Set to true if the notification was hidden by the user

### is_dismissable
One of true (default) or false

If set to false no application except for the one that created the notification should remove the notification from the ddb.
Dismissing means hiding.

This is useful for ongoing jobs like downloads, calls or timers or for notifications that only get updated once in a while but may still contain relevant information and should be set to false once the contains relevent information that the user may want to hide state is over.

### time_last_updated
The unix timestamp (UTC) of when the notification was last updated with information relevant for the user (title, description, progress)

### progress_steps
An integer progress indicator starting at 0 and going until to whatever progress_steps_total is set to, this should be shown as an additional indicator above all other progress indicators

This should be used if multiple different operations are part of one longer running process, like downloading, veryfying, extracting, installing and similar.
A download verify and extract pipeline would be 3 steps.

This indicator should not be used if there are multiple of the same job like compressing 100 files, except when for some reason the processing is never done in parallel and there is only one step of processing

### progress_steps_total
The total amount of steps that have to be thaken before a goal is reached

### progress_percent
A value in the usual percent range from 0 to 100 that can be used for all kind of progress indicator (or as a hack for a valume indicator or brightness)

## progress…
There are more progrss values with different semantics to be defined like time, count and bytes to allow for a nice display of all of them.

### command_action:<command_id>
Command ids range from 1 to 5, any reasonable application notification design should use at most 3 commands for everyday notifications

using command action fields commands can be mapped to actions to be thaken
These action fields have a command then a space and then one or more arguments (similar to a shell command)

Available actions are:
* signal - sends a signal with the name of command_action:<command_id> to this notification object when triggered
* uri &lt;uri&gt; - instructs the ui to open an uri

### command_label:<command_id>
using command_label fields commands can be given labels that are localizable and in general more human friendly

### command_name:<command_id>
using command_name fields commands can be put into general categorys and localized on the notification UI end (i.e. for using abbreviations) or be entirely be replaced with icon buttons.

### uri
May contain an uri to the resource this notification is about like a wheater report or an irc channel

### class
An identifier that can be used to group similar notifications

* message
* call.incoming
* call.ongoing
* alarm
* wheater
* wheater.warning
* download.ongoing
* download.finished
* screenshot
